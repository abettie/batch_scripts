<?php
/**
 * 引き算メーカー
 */
$former_min = $argv[1]; // 計算されるほうの最小値
$former_max = $argv[2]; // 計算されるほうの最大値
$latter_min = $argv[3]; // 計算するほうの最大値
$latter_max = $argv[4]; // 計算するほうの最大値
$result_min = $argv[5]; // 期待される計算結果の最小値
$result_max = $argv[6]; // 期待される計算結果の最大値
$problem_count = $argv[7]; // 出力する問題の数

$count = 0;
while ($count < $problem_count) {
    $former = rand($former_min, $former_max);
    $latter = rand($latter_min, $latter_max);
    $result = $former - $latter;
    if ($result > $result_max || $result < $result_min) {
        // 計算の答えが範囲外
        continue;
    }
    // 計算式出力
    echo $former.'-'.$latter.'='."\n";
    $count++;
}
