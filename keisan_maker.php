<?php
/**
 * 計算問題メーカー
 */
$operator = $argv[1]; // 演算子
$former_min = $argv[2]; // 計算されるほうの最小値
$former_max = $argv[3]; // 計算されるほうの最大値
$latter_min = $argv[4]; // 計算するほうの最大値
$latter_max = $argv[5]; // 計算するほうの最大値
$repeat_count = (isset($argv[6])) ? $argv[6] : 1; // 組み合わせを何回繰り返すか
for ($i=0; $i<$repeat_count; $i++) {
    $used = [];
    $count = 0;
    while (
        // 組み合わせがなくなるまで続行
        $count < calcCombinationNumber(
            $former_min,
            $former_max,
            $latter_min,
            $latter_max
        )
    ) {
        $f = rand($former_min, $former_max);
        $l = rand($latter_min, $latter_max);
        if (isset($used[$f][$l])) {
            continue;
        }
        // 数式の出力
        echo $f.$operator.$l.'='."\n";
        $used[$f][$l] = true;
        $count++;
    }
}

/**
 * 組み合わせの数の計算
 *
 * @param int $former_min される側最小値
 * @param int $former_max される側最大値
 * @param int $latter_min する側最小値
 * @param int $latter_max される側最大値
 *
 * @return int 組み合わせの数
 */
function calcCombinationNumber($former_min, $former_max, $latter_min, $latter_max)
{
    $former_count = $former_max - $former_min + 1;
    $latter_count = $latter_max - $latter_min + 1;
    return $former_count * $latter_count;
}
