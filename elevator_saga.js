{
    init: function(elevators, floors) {
        // 各階のエレベーターホールで押されたボタン
        let pressedHallButtons = [];
        let maxFloorNum = 0;

        floors.forEach(function(floor) {
            // ボタン押下状態初期化
            const floorNum = floor.floorNum();
            pressedHallButtons[floorNum] = {
                up: false,
                down: false
            };
            maxFloorNum = floor.floorNum();
            floor.on("up_button_pressed", function() {
                pressedHallButtons[this.floorNum()]["up"] = true;
            });
            floor.on("down_button_pressed", function() {
                pressedHallButtons[this.floorNum()]["down"] = true;
            });
        });

        // Whenever the elevator is idle (has no more queued destinations) ...
        elevators.forEach(function(elevator, elevatorIndex) {
            // 現在の進行方向。頂上に着くまでは上方向のまま、1階に戻るまでは下方向のまま
            elevator.direction = "up";
            elevator.getReverseDirection = function () {
                if (this.direction === "up") {
                    return "down";
                } else {
                    return "up";
                }
            };
            console.log("elevator["+elevatorIndex+"]'s maxPassengerCount: "+elevator.maxPassengerCount());
            elevator.on("idle", function() {
                const pressedFloors = elevator.getPressedFloors();

                // エレベータ内のボタン
                let nextFloorInElevator = {
                    up: maxFloorNum,
                    down: 0
                };
                let nextFloorInElevatorDecided = {
                    up: false,
                    down: false
                };
                if (pressedFloors.length > 0) {
                    pressedFloors.forEach(function(floorNum) {
                        // 押されたボタンは昇順
                        if (nextFloorInElevatorDecided.up === false && floorNum > elevator.currentFloor()) {
                            // 今より上の階で最も近い階を取得
                            nextFloorInElevator.up = floorNum;
                            nextFloorInElevatorDecided.up = true;
                        }
                        if (floorNum < elevator.currentFloor()) {
                            // 今より下の階で最も近い階を取得
                            nextFloorInElevator.down = floorNum;
                            nextFloorInElevatorDecided.down = true;
                        }
                    });
                }
                // 各階のボタン
                let nextFloorHall = {
                    up: maxFloorNum,
                    down: 0
                };
                let nextFloorHallDecided = {
                    up: false,
                    down: false
                };
                if (elevator.loadFactor() < 0.5) {
                    // 人員に余裕がある場合は、フロアーのボタンを気にする
                    pressedHallButtons.forEach(function (button, floorNum) {
                        if (button.up === true || button.down === true) {
                            if (nextFloorHallDecided.up === false && floorNum > elevator.currentFloor()) {
                                // 今より上の階で最も近い階を取得
                                nextFloorHall.up = floorNum;
                                nextFloorHallDecided.up = true;
                            }
                            if (floorNum < elevator.currentFloor()) {
                                // 今より下の階で最も近い階を取得
                                nextFloorHall.down = floorNum;
                                nextFloorHallDecided.down = true;
                            }
                        }
                    });
                }
                // エレベーター内のボタンと各階のボタンのうち、近いほうを採用
                const nextFloor = {
                    up: Math.min(nextFloorInElevator.up, nextFloorHall.up),
                    down: Math.max(nextFloorInElevator.down, nextFloorHall.down)
                };
                let nextFloorToGo = elevator.currentFloor();
                if (nextFloorInElevatorDecided[elevator.direction] === false
                 && nextFloorHallDecided[elevator.direction] === false) {
                    nextFloorToGo = nextFloor[elevator.getReverseDirection()];
                    elevator.direction = elevator.getReverseDirection();
                } else {
                    nextFloorToGo = nextFloor[elevator.direction];
                }
                elevator.goToFloor(nextFloorToGo);
                pressedHallButtons[nextFloorToGo]["up"] = false;
                pressedHallButtons[nextFloorToGo]["down"] = false;
                if (nextFloorToGo === 0) {
                    // 進行方向を上に変える
                    elevator.direction = "up";
                }
                if (nextFloorToGo === maxFloorNum) {
                    // 進行方向を下に変える
                    elevator.direction = "down";
                }
            });

            elevator.on("passing_floor", function(floorNum, direction) {
                //console.log(floorNum+' '+direction);
            });

            elevator.on("floor_button_pressed", function(floorNum) {
                // Maybe tell the elevator to go to that floor?
                //console.log(floorNum+' pressed.');
            });

            elevator.on("stopped_at_floor", function(floorNum) {
                // Maybe decide where to go next?
            });
        });
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
